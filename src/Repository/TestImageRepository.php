<?php

namespace App\Repository;

use App\Entity\TestImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TestImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestImage[]    findAll()
 * @method TestImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TestImage::class);
    }

    // /**
    //  * @return TestImage[] Returns an array of TestImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestImage
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
