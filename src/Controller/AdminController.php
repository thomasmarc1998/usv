<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ContenuArticle;
use App\Entity\TestImage;
use App\Form\ArticleType;
use App\Form\ArticleType1Type;
use App\Form\FormCreateIType;
use App\Form\FormeCreateIType;
use App\Form\ImageType;
use App\Repository\ArticleRepository;
use App\Repository\ContenuArticleRepository;
use App\Repository\TestImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use App\Form\AjoutMajType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormTypeInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
* Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @IsGranted("ROLE_ADMIN")
 **/
class AdminController extends AbstractController
{
    /**
     * @Route("/admin/home", name="admin_home")
     */
    public function showAdmin(ArticleRepository $repo, TestImageRepository $repo1)
    {
        $articles=$repo->findAll();
        $images=$repo1->findAll();
        return $this->render('administration/administration.html.twig', [
            'controller_name' => 'AdminController','articles'=>$articles,'images'=>$images
        ]);
    }
    /**
     * @Route("/admin/newI", name="createImage")
     * @route("/admin/{id}/editI", name="editImage")
     */
    public function formI(TestImage $i=null,Request $request,ObjectManager $manager,ArticleRepository $repo, TestImageRepository $repo1)
    {
        $articles=$repo->findAll();
        $images=$repo1->findAll();
        if(!$i)
        {
            $i=new TestImage();
        }
        $form =$this->createForm(FormCreateIType::class,$i);
        $form->handleRequest($request);
        // we must transform the image string from Db  to File to respect the form types

        if($form->isSubmitted()&&$form->isValid()){
            $manager->persist($i);
            $manager->flush();
            return $this->redirectToRoute('admin_home');
        }
        dump($i);
        return $this->render('administration/formCreateI.html.twig',['AdminController','articles'=>$articles,'images'=>$images,'formCreateI'=>$form->createView(),'editMode'=>$i->getId()!==null]);
    }
    /**
     * @Route("/admin/new", name="create")
     * @route("/admin/{id}/edit", name="edit")
     */
    public function formEdit(Article $article=null,Request $request,ObjectManager $manager,ArticleRepository $repo, TestImageRepository $repo1)
    {
        $articles=$repo->findAll();
        $images=$repo1->findAll();
       if(!$article)
        {
            $article=new Article();
        }
        $form =$this->createForm(ArticleType::class,$article);
        $form->handleRequest($request);
        // we must transform the image string from Db  to File to respect the form types

        if($form->isSubmitted()&&$form->isValid()){

            $manager->persist($article);
            $manager->flush();
            return $this->redirectToRoute('blog_show',['id'=>$article->getId()]);
        }
        dump($article);

        return $this->render('administration/formCreate.html.twig',['AdminController','articles'=>$articles,'images'=>$images,'formArticle'=>$form->createView(),'editMode'=>$article->getId()!==null ]);
    }
    /**
     * @Route("/admin/{id}/delete", name="delete")
     */
    public function delete(Article $article=null,ObjectManager $manager)
    {
        if(!$article)
        {
            return $this->redirectToRoute('home');
        }
        $manager->remove($article);
        $manager->flush();
        return $this->redirectToRoute('admin_home');
    }

    /**
     * @Route("/admin/{id}/deleteI", name="deleteImage")
     */
    public function deleteI(TestImage $image=null,ObjectManager $manager)
    {
        if(!$image)
        {
            return $this->redirectToRoute('home');
        }
        $manager->remove($image);
        $manager->flush();
        return $this->redirectToRoute('admin_home');
    }
    /**
     * @Route("/admin/deleteAll", name="deleteAll")
     */
    public function deleteAll(Article $article=null,ObjectManager $manager,ArticleRepository $repo)
    {
        $arrayArticles=$repo->findAll();
         foreach($arrayArticles  as $article)
         {
             $manager->remove($article);
         }
         return $this->redirectToRoute('home');
    }
}
