<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Form\SearcheType;
use App\Repository\ArticleRepository;
use App\Repository\TestImageRepository;
use MongoDB\Driver\Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use App\Form\AjoutMajType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormTypeInterface;
use App\Form\EditContenuType;
use App\Entity\ContenuArticle;
use App\Twig\AppExtension;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(ArticleRepository $repo,TestImageRepository $repo1)
    {
        $images=$repo1->findAll();
        $articles = $this->getDoctrine()
        ->getRepository(Article::class)
        ->findByDate();
        return $this->render('main/home.html.twig', [
            'controller_name' => 'MainController','articles'=>$articles,'images'=>$images
        ]);
    }
    /**
     * @Route("/vieAssos", name="vieAssociative")
     */
    public function vieAssociative(ArticleRepository $repo,TestImageRepository $repo1)
    {
        $articles=$repo->findAll();
        $images=$repo1->findAll();
        return $this->render('main/vieAssociative.html.twig', [
            'controller_name' => 'MainController','articles'=>$articles,'images'=>$images
        ]);
    }
    /**
     * @Route("/actualites", name="actualites")
     */
    public function ShowActualites( Article $article=null,ArticleRepository $repo,TestImageRepository $repo1,Request $request)
    {
        $images=$repo1->findAll();


            $form =$this->createForm(SearcheType::class,$article);
            $form->handleRequest($request);
        $articles = $this->getDoctrine();
        $title = $form['title']->getData();
        if($form->isSubmitted()&&$form->isValid()) {
            dump($articles);
            $articles = $this->getDoctrine()
                ->getRepository(Article::class)
                ->findEntitiesByString($title);


            return $this->render('main/actualites.html.twig', [
                'controller_name' => 'MainController','articles'=>$articles,'images'=>$images,'formSearch'=>$form->createView()]);
        }
        return $this->render('main/actualites.html.twig', [
            'controller_name' => 'MainController','articles'=>$articles,'images'=>$images,'formSearch'=>$form->createView()
        ]);
    }
    /**
     * @Route("/actualitesTag", name="actualitesTag")
     */
    public function ShowActualitesTag( Article $article=null,ArticleRepository $repo,TestImageRepository $repo1,Request $request)
    {
        $images=$repo1->findAll();
        $articles = $this->getDoctrine();
        $categorie =$request->query->get('LaCategorie');
            dump($articles);
            $articles = $this->getDoctrine()
                ->getRepository(Article::class)
                ->findEntitiesByString($categorie);

        return $this->render('main/actualites.html.twig', [
            'controller_name' => 'MainController','articles'=>$articles,'images'=>$images
        ]);
    }
    /**
     * @Route("/card", name="card")
     */
    public function ShowCard(ArticleRepository $repo,TestImageRepository $repo1)
    {
        $articles=$repo->findAll();
        $images=$repo1->findAll();
        return $this->render('main/card.html.twig', [
            'controller_name' => 'MainController','articles'=>$articles,'images'=>$images
        ]);
    }
    /**
     * @Route("main/show/{id}", name="blog_show")
     */
    public  function  show(Article $article,Request $request,ArticleRepository $repo,TestImageRepository $repo1)
    {
        $articles=$repo->findAll();
        $images=$repo1->findAll();
       return $this->render('main/show.html.twig',['article'=>$article,'articles'=>$articles,'images'=>$images]);
    }
    /**
     * @Route("main/error", name="error")
     */
    /*public  function  error()
    {
        return $this->render('main/Erreur.html.twig');
    }

    /**
     * Creates a new ActionItem entity.
     *
     * @Route("/search", name="ajax_search")
     * @Method("GET")
     */
   /* public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requestString = $request->get('q');
        $entities =  $em->getRepository('App:Article')->findEntitiesByString($requestString);
        if(!$entities) {
            $result['entities']['error'] = "keine Einträge gefunden";
        } else {
            $result['entities'] = $this->getRealEntities($entities);
        }
        return new Response(json_encode($result));
    }
    public function getRealEntities($entities){
        foreach ($entities as $entity){
            $realEntities[$entity->getId()] = $entity->getTitle();
        }
        return $realEntities;
    }*/
}

?>
