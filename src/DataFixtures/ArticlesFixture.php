<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

class ArticlesFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=1;$i<=10;$i++)
        {
            $article=new Article();
            $article->setTitle("Titre de l'article")
                ->setContent("<p>Contenu de l'artcile n°($i")
                ->setContenu("<p>Description de l'artcile n°($i")
            ->setCreateAt(new \DateTime());
            $manager->persist($article);

        }

        $manager->flush();
    }
}
