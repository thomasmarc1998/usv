<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lib_categ;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="LaCategorie")
     */
    private $categ;

    public function __construct()
    {
        $this->categ = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibCateg(): ?string
    {
        return $this->lib_categ;
    }

    public function setLibCateg(string $lib_categ): self
    {
        $this->lib_categ = $lib_categ;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getCateg(): Collection
    {
        return $this->categ;
    }

    public function addCateg(Article $categ): self
    {
        if (!$this->categ->contains($categ)) {
            $this->categ[] = $categ;
            $categ->setLaCategorie($this);
        }

        return $this;
    }

    public function removeCateg(Article $categ): self
    {
        if ($this->categ->contains($categ)) {
            $this->categ->removeElement($categ);
            // set the owning side to null (unless already changed)
            if ($categ->getLaCategorie() === $this) {
                $categ->setLaCategorie(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->lib_categ;
    }
}
