<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *  fields= {"login"},
 *  message="le login que vous avez indiqu est déja utilisé!")
 *  )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8",minMessage="Votre mot de passe doit faire minimum 8 caractéres")
     * @Assert\EqualTo(propertyPath="confirm_password", message="votre mot de passe doit être le même")
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="votre mot de passe doit être le même")
     */

    public  $confirm_password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $droit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    //////////////////////////////////////////////////////////
    public  function  eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }
    public function getRoles()
    {
        if($this->getDroit()==0)
        {
            return ['ROLE_USER'];
        }
        else{ return ['ROLE_ADMIN'];}

    }
    public function getUsername()
    {
       /* if(this->)
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';*/
    }

    public function getDroit(): ?bool
    {
        return $this->droit;
    }

    public function setDroit(bool $droit): self
    {
        $this->droit = $droit;

        return $this;
    }
}
