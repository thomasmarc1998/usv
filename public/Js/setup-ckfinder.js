window.onload = function () {
    if (window.CKEDITOR){
        var path = '/ckfinder/connector';
        CKFinder.config( { connectorPath: (window.location.pathname.indexOf("app_dev.php") == -1) ? path : '/app_dev.php'+path} );
        for (var ckInstance in CKEDITOR.instances){
            CKFinder.setupCKEditor(CKEDITOR.instances[ckInstance]);
        }
    }
}